<?php
namespace App\Model;
use PDO;
use PDOException;

class Database{
    public $host ="localhost";
    public $dbname ="atomic_project_b35";
    public $user="root";
    public $pass="";
    public function __construct()
    {
        try {
            $DBH = new PDO("mysql:host=$this->host;dbname=$this->dbname", $this->user, $this->pass);
        }
        catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}
